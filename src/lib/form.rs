use std::collections::HashMap;

use crate::{form_options::FormOptions, input_base::InputBase};

#[derive(Default)]
pub struct Form<T: InputBase + Default> {
    inputs: Vec<T>,
    _options: FormOptions,
}

impl<T> Form<T>
where
    T: InputBase + Default,
{
    pub fn new(form_options: FormOptions) -> Self {
        Self {
            _options: { form_options },
            ..Default::default()
        }
    }

    pub fn add_input(&mut self, input: T) {
        self.inputs.push(input);
    }

    pub fn draw() {
        println!("form draw")
    }

    pub fn prompt(&mut self) {
        // let highest_label_len = if self.options.align_answers {
        //     self.inputs
        //         .iter()
        //         .map(|i| i.get_label().len())
        //         .max()
        //         .unwrap()
        // } else {
        //     0
        // };

        self.inputs.iter_mut().for_each(|i| {
            i.draw().unwrap();
            i.prompt().unwrap();
        });
    }

    pub fn get_values(&self) -> HashMap<String, String> {
        let mut hashmap = HashMap::new();
        &self.inputs.iter().for_each(|i| {
            hashmap.insert(i.get_name(), i.get_value());
        });
        hashmap
    }
}
