use std::io::Result;

pub trait InputBase {
    fn set_value(&mut self, value: String);
    fn draw(&self) -> Result<()>;
    fn prompt(&mut self) -> Result<()>;
    fn validate(&self, value: String) -> std::result::Result<String, String>;
    fn get_name(&self) -> String;
    fn get_value(&self) -> String;
    fn get_label(&self) -> String;
}
