use std::io::{stdin, stdout, Result, Write};

use crate::{
    input_base::InputBase,
    input_type::{InputType, NumberType},
};

pub struct InputText<'a> {
    name: &'a str,
    label: &'a str,
    value: String,
    variant: InputType<'a>,
}

impl<'a> InputText<'a> {
    pub fn new(name: &'a str, label: &'a str, variant: InputType<'a>) -> Self {
        Self {
            name,
            label,
            variant,
            ..Default::default()
        }
    }
}

impl<'a> InputBase for InputText<'a> {
    fn set_value(&mut self, value: String) {
        match self.validate(value) {
            Ok(result) => {
                self.value = result;
            }
            Err(err) => {
                stdout()
                    .write(format!("\n! -> {}", err).as_bytes())
                    .unwrap();
                stdout().flush().unwrap();
                stdout().write("\n\n".as_bytes()).unwrap();
                stdout().flush().unwrap();
                self.draw().unwrap();
                self.prompt().unwrap();
            }
        }
    }

    fn draw(&self) -> Result<()> {
        stdout().write(format!("{} ", self.label).as_bytes())?;
        stdout().flush().unwrap();
        Ok(())
    }

    fn validate(&self, value: String) -> std::result::Result<String, String> {
        match &self.variant {
            InputType::Bool(true_label) => {
                if *true_label == value {
                    return Ok("true".into());
                } else {
                    return Ok("false".into());
                }
            }
            InputType::Number(number_type) => match number_type {
                NumberType::Float => {
                    if let Err(_) = value.parse::<f64>() {
                        return Err("cannot parse value as a floating number".into());
                    } else {
                        Ok(value)
                    }
                }
                NumberType::Int => {
                    if let Err(_) = value.parse::<u64>() {
                        return Err("cannot parse value as integer".into());
                    } else {
                        Ok(value)
                    }
                }
            },
            InputType::Text => Ok(value),
        }
    }

    fn prompt(&mut self) -> Result<()> {
        let mut val = String::new();
        stdin().read_line(&mut val)?;
        self.set_value(val.trim().to_owned());
        Ok(())
    }

    fn get_name(&self) -> String {
        self.name.to_owned()
    }

    fn get_value(&self) -> String {
        self.value.to_owned()
    }

    fn get_label(&self) -> String {
        self.label.to_owned()
    }
}

impl<'a> Default for InputText<'a> {
    fn default() -> Self {
        Self {
            name: "name not defined",
            label: "label not defined",
            value: String::default(),
            variant: InputType::default(),
        }
    }
}
