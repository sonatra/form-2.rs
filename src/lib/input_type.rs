pub enum NumberType {
    Int,
    Float,
}
pub enum InputType<'a> {
    /// For boolean value
    /// The matching response for a true result
    Bool(&'a str),
    Text,
    Number(NumberType),
}

impl<'a> Default for InputType<'a> {
    fn default() -> Self {
        Self::Text
    }
}
