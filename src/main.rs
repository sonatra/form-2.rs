use lib::{
    form::Form,
    form_options::FormOptions,
    input_text::InputText,
    input_type::{InputType, NumberType},
};
use serde::Deserialize;

#[derive(Deserialize, Debug)]
struct FormValues {
    name: String,
    age: u8,
    comf: bool,
}

fn main() {
    let mut form = Form::new(FormOptions::default());
    form.add_input(InputText::new("name", "Your name ?", InputType::Text));
    form.add_input(InputText::new(
        "age",
        "Your age ?",
        InputType::Number(NumberType::Int),
    ));
    form.add_input(InputText::new(
        "feeling",
        "Are you feeling good ? (Y/n)",
        InputType::Bool("Y"),
    ));
    form.prompt();
    println!("{:?}", form.get_values())
}
